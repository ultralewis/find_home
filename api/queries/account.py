from pydantic import BaseModel
from typing import Union
from queries.pool import pool


class AccountBasicInfo(BaseModel):
    first_name: str
    last_name: str
    email: str

class AccountRegisterIn(AccountBasicInfo):
    username: str
    password: str


class AccountRegisterOut(AccountBasicInfo):
    id: int
    username: str

class ChangePassword(BaseModel):
    old_password: str
    new_password: str

class Error(BaseModel):
    message: str

class AccountRepository:
    def account_register_in_to_out(self, id: int, account: AccountRegisterIn):
        old_data = account.dict()
        return AccountRegisterOut(id=id, **old_data)

    def patch_account(self, id: int, username: str, account: AccountBasicInfo):
        old_data = account.dict()
        return AccountRegisterOut(id=id, username=username, **old_data)

    #! Need to add password encrpytion
    def create_account(self, account: AccountRegisterIn) -> Union[AccountRegisterOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO account
                        (username, password, first_name, last_name, email)
                        VALUES (%s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            account.username,
                            account.password,
                            account.first_name,
                            account.last_name,
                            account.email,
                        ],
                    )
                    id = result.fetchone()[0]
                    return self.account_register_in_to_out(id, account)

        except Exception as e:
            return {"message": e}

    def get_account(self, account_id: int) -> Union[Error, AccountRegisterOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT *
                        from account
                        where id = %s;
                        """,
                        [account_id],
                    )
                    result = db.fetchone()
                    record = None
                    if result is not None:
                        record = {}
                        for i, col in enumerate(db.description):
                            record[col.name] = result[i]
                    record.pop("password")
                    return record
        except Exception as e:
            return {"message": e}

    def update_account(self, account_id, account_data: AccountBasicInfo) -> Union[Error, AccountRegisterOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE account
                        SET first_name = %s
                        , last_name = %s
                        , email = %s
                        WHERE id = %s
                        RETURNING username;
                        """,
                        [
                            account_data.first_name,
                            account_data.last_name,
                            account_data.email,
                            account_id
                        ],
                    )
                    username = db.fetchone()[0]
                    return self.patch_account(account_id, username, account_data)

        except Exception as e:
            return {"message": e}

    def delete_account(self, account_id):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM account
                        where id = %s
                        """,
                        [account_id],
                    )
        except Exception as e:
            return {"message": e}
