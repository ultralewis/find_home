from pydantic import BaseModel
from typing import Union, Dict
from datetime import date
from queries.pool import pool
import csv

class Error(BaseModel):
    message: str

class SearchCity(BaseModel):
    city: str
    from_date: date
    to_date: date
    frequency: str


class ChartRepository:
    def get_city_data(self, city: str, frequency: str):
        if frequency == "weekly":
            with open('/app/static/metro_median_sale_price_uc_sfr_sm_sa_week.csv', mode = 'r') as file:
                csvFile = csv.DictReader(file)
                list_csvFile = list(csvFile)

                for city1 in list_csvFile:
                    if city.lower() in city1["RegionName"].lower():
                        filtered_city = {city: city1}

        elif frequency == "monthly":
            with open('/app/static/metro_median_sale_price_uc_sfr_sm_sa_month.csv', mode = 'r') as file:
                csvFile = csv.DictReader(file)
                list_csvFile = list(csvFile)

                for city1 in list_csvFile:
                    if city.lower() in city1["RegionName"].lower():
                        filtered_city = {city: city1}

        return filtered_city
