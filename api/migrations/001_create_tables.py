steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE account (
            id SERIAL NOT NULL UNIQUE,
            username VARCHAR(25) NOT NULL UNIQUE,
            password VARCHAR(60) NOT NULL,
            first_name VARCHAR(50) NOT NULL,
            last_name VARCHAR(50) NOT NULL,
            email VARCHAR(50) NOT NULL UNIQUE
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE account;
        """,
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE address (
            id SERIAL NOT NULL UNIQUE,
            street1 VARCHAR(100) NOT NULL,
            street2 VARCHAR(50),
            city VARCHAR(50) NOT NULL,
            state VARCHAR(2) NOT NULL,
            zipcode VARCHAR(5) NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE address;
        """
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE search_results (
            id SERIAL NOT NULL UNIQUE,
            account_id INT REFERENCES account(id) ON DELETE CASCADE,
            address_id INT REFERENCES address(id) ON DELETE CASCADE,
            down_payment INT NOT NULL CHECK (down_payment >= 0),
            credit_score INT NOT NULL CHECK (credit_score >= 300),
            loan_term INT NOT NULL CHECK (loan_term >= 120),
            monthly_pay INT NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE search_results;
        """
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE address_pics (
            id SERIAL NOT NULL UNIQUE,
            address_id INT REFERENCES address(id),
            pic_url VARCHAR(500) NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE address_pics;
        """
    ]
]
