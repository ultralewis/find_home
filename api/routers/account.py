from fastapi import (
    APIRouter,
    Depends,
    Response,
    HTTPException,
    status,
    Request,
)
from typing import Union
from queries.account import AccountRegisterIn, AccountRegisterOut, Error, AccountRepository, AccountBasicInfo
from pydantic import BaseModel

router = APIRouter()


@router.post("/api/account/", response_model=Union[AccountRegisterOut, Error])
async def create_account(account: AccountRegisterIn, response: Response, queries: AccountRepository = Depends()):
    try:
        result = queries.create_account(account)

    except Exception:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create an account with those credentials.",
        )

    return result

@router.get("/api/account/{account_id}", response_model=Union[Error, AccountRegisterOut])
def get_account(account_id:int, response: Response, queries: AccountRepository = Depends()):
    record = queries.get_account(account_id)

    if record is None:
        response.status_code = 404
    else:
        return record

@router.patch("/api/account/{account_id}", response_model = Union[Error, AccountRegisterOut])
def patch_account(account_data: AccountBasicInfo, account_id: int, response: Response, queries: AccountRepository = Depends()) -> Union[Error, AccountRegisterOut]:
    result = queries.update_account(account_id, account_data)
    if result is None:
        response.status_code = 404
    else:
        return result

@router.delete("/api/account/{account_id}", response_model = bool)
async def delete_account(account_id: int, queries: AccountRepository = Depends()) -> bool:
    result = queries.delete_account(account_id)

    if result is True:
        return result
    else:
        return False
