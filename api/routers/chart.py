from fastapi import (
    APIRouter,
    Depends,
    Response,
    HTTPException,
    status,
    Request,
)
from typing import Union
from queries.chart import Error, SearchCity, ChartRepository
from pydantic import BaseModel

router = APIRouter()

@router.get("/api/chart/")
def get_city_data(city: str, frequency: str, response: Response, queries: ChartRepository = Depends()):

    record = queries.get_city_data(city, frequency)
    print("***** record: ", record)
    return record
